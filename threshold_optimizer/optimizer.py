import numpy as np
import pandas as pd
from functools import partial


def optimize_threshold(scores, actual_classes):
    """The optimization makes use of the fact
        that very often, when we sort the scores and corresponding actual labels, the range where
        the classes overlay is narrow. Therefore, the threshold value does influence 
        the output labels only in a narrow range of values (where the classes are mixed).
        Thus, using this fact, the search space can be narrowed down. 

    Args:
        scores (array_like): array_like object that contains probability scores
        actual_classes (array_like): array_like object that contains ground truth labels

    Returns:
        float: Optimized threshold with regards to correctly classifed samples fraction
    """

    actual_classes = np.array(actual_classes)
    scores = np.array(scores)

    scores, actual_classes = _sort_arrays(scores,actual_classes)
    
    
    threshold_space = np.concatenate(([0], scores))
  
    # Divide the array into chunks having identical consecutive elements
    consecutive_chunks = _split_consecutive(actual_classes)

    if len (consecutive_chunks) >= 2:
        left = consecutive_chunks[0]
        right = consecutive_chunks[-1]
        threshold_space_idx = (len(left), len(actual_classes) - len(right) + 1)
    
    else:
        if consecutive_chunks[0][0] == 1:
            return threshold_space[0]
        else:
            return threshold_space[len(threshold_space) - 1]
    
    threshold_space = threshold_space[threshold_space_idx[0]:threshold_space_idx[1]]
    
    threshold_func = partial(_threshold_to_accuracy, scores, actual_classes)

    accs = np.array(list(map(threshold_func, threshold_space)))

    return threshold_space[np.argmax(accs)]

        
def _optimize_threshold_naive(scores, actual_classes):
    """Naive implementation of threshold optimizer that iterates 
        through all elements of scores array

    Args:
        scores (array_like): array_like object that contains probability scores
        actual_classes (array_like): array_like object that contains ground truth labels

    Returns:
        float: Optimized threshold with regards to correctly classifed samples fraction
    """

    actual_classes = np.array(actual_classes)
    scores = np.array(scores)
    scores, actual_classes = _sort_arrays(scores,actual_classes)
    threshold_space = np.concatenate(([0], scores))

    threshold_func = partial(_threshold_to_accuracy, scores, actual_classes)

    accs = np.array(list(map(threshold_func, threshold_space)))

    return threshold_space[np.argmax(accs)]


def _split_consecutive(data):
    """Divide the array into chunks having identical consecutive elements e.g.:
    [-1, -1, -1, 1, -1, -1, 1, 1, 1] -> [[-1, -1, -1], [1], [-1, -1], [1, 1, 1]]

    Args:
        data (np.array): array to split

    Returns:
        list[np.array]: splitted array
    """
    return np.split(data, np.where(np.diff(data) != 0)[0] + 1)
 

def _calc_accuracy(y_true, y_pred):
    """Calulate fraction of correctly classified samples

    Args:
        y_true (array_like): ground truth labels
        y_pred (array_like): model output labels

    Returns:
        float: fraction of correctly classified labels (accuracy)
    """
    return (y_true == y_pred).mean()


def _sort_arrays(arr1, arr2):
    """Sorts two arrays by elements of the first array 

    Args:
        arr1 (np.array): first array that determines the order
        arr2 (np.array): second array

    Returns:
        (np.array, np.array): a tuple for sorted arrays
    """
    order = np.argsort(arr1)
    arr1_sorted = arr1[order]
    arr2_sorted = arr2[order]
    return arr1_sorted, arr2_sorted


def _threshold_to_accuracy(scores, actual_classes, threshold):
    """Calulates accuracy for a given threshold

    Args:
        scores (array_like): array_like object that contains probability scores
        actual_classes (array_like): array_like object that contains ground truth labels
        threshold (float): threshold value

    Returns:
        float: fraction of correctly classified labels (accuracy)
    """

    pos = np.ones_like(scores)
    neg = pos * -1
    y_pred = np.where(scores > threshold, pos, neg)
    return _calc_accuracy(actual_classes, y_pred)







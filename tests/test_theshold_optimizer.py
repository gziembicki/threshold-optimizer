import pytest
import numpy as np
from random import shuffle
from time import time
from threshold_optimizer.optimizer import optimize_threshold, _split_consecutive, _optimize_threshold_naive



def test_simple():
    scores = [0.1, 0.3, 0.4, 0.7]
    actual_classes = [-1, -1, 1, 1]
    res = optimize_threshold(scores, actual_classes)
    assert res == 0.3

def test_complex():
    scores = [0.1, 0.3, 0.34, 0.36, 0.38, 0.4, 0.45, 0.47, 0.55, 0.6, 0.7, 0.75]
    actual_classes = [-1, -1, -1, -1, 1, 1, -1, 1, 1, 1, 1, 1]
    res0 = _optimize_threshold_naive(scores, actual_classes)
    res1 = optimize_threshold(scores, actual_classes)

    assert res0 == res1

def test_complex2():
    scores = [0.1, 0.3, 0.34, 0.36, 0.38, 0.4, 0.41, 0.42, 0.43, 0.45, 0.46, 0.47, 0.55, 0.6, 0.7, 0.75]
    actual_classes = [-1, -1, -1, -1, 1, -1, -1, 1, -1, 1, -1, 1, 1, 1, 1, 1]
    assert len(scores) == len(actual_classes)
    res0 = _optimize_threshold_naive(scores, actual_classes)
    res1 = optimize_threshold(scores, actual_classes)
    print(res0)
    assert res0 == res1

def test_big():
    t0 = time()
    scores = np.array(range(1000000))/1000000
    actual_classes = [-1]*500000 + [1]*500000
    assert len(scores) == len(actual_classes)
    res1 = optimize_threshold(scores, actual_classes)
    t1 = time()
    print(f"Computing time: {t1 - t0}s")

def test_complex_unsorted():

    def shuffle_in_unison_scary(a, b):
        rng_state = np.random.get_state()
        np.random.shuffle(a)
        np.random.set_state(rng_state)
        np.random.shuffle(b)
        return a, b


    scores = [0.1, 0.3, 0.34, 0.36, 0.38, 0.4, 0.41, 0.42, 0.43, 0.45, 0.46, 0.47, 0.55, 0.6, 0.7, 0.75]
    actual_classes = [-1, -1, -1, -1, 1, -1, -1, 1, -1, 1, -1, 1, 1, 1, 1, 1]
    assert len(scores) == len(actual_classes)

    scores_shuffled, actual_classes_shuffled = shuffle_in_unison_scary(scores, actual_classes)
    res0 = _optimize_threshold_naive(scores_shuffled, actual_classes_shuffled)
    res1 = optimize_threshold(scores_shuffled, actual_classes_shuffled)

    assert res0 == res1

def test_corner_case_1():
    scores = [0.1, 0.3, 0.4, 0.7]
    actual_classes = [1, 1, 1, 1]
    res0 = _optimize_threshold_naive(scores, actual_classes)
    res1 = optimize_threshold(scores, actual_classes)

    assert res0 == res1 == 0.0

def test_corner_case_0():
    scores = [0.1, 0.3, 0.4, 0.7]
    actual_classes = [-1, -1, -1, -1]
    res0 = _optimize_threshold_naive(scores, actual_classes)
    res1 = optimize_threshold(scores, actual_classes)

    assert res0 == res1 == 0.7


def test_consecutive_simple():
    array = np.array([-1,-1, 1,1])
    cons = _split_consecutive(array)

    assert (cons[0] == np.array([-1,-1])).all()
    assert (cons[1] == np.array([1, 1])).all()
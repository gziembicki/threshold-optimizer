# Threshold Optimizer

This is a module that optimizes threshold value to maximize accuracy of a Machine Learning model

Implementation of threshold optimizer makes use of the fact that very often, when we sort the scores and corresponding actual labels, the range where the classes overlay is narrow. Therefore, the  threshold value does influence the output labels only in a narrow range of values (where the classes are mixed). Thus, using this fact, the search space can be narrowed down. Please see the classification.png image.

## Getting Started


### Installing

You can install the package from the test PyPI repository

```
pip install -i https://test.pypi.org/simple/ threshold-optimizer==0.0.3
```


### Examples

Import the appropriate module into your project

```
from threshold_optimizer.optimizer import optimize_threshold
```

Calculate optimal threshold
```
scores = [0.1, 0.3, 0.4, 0.7]
actual_classes = [-1, -1, 1, 1]
optimize_threshold(scores, actual_classes)
```

### Development and tests

Clone project 

```
git clone https://gitlab.com/gziembicki/threshold-optimizer.git
```

For local development or testing create a virtual environemnt and install the package

```
cd threshold_optimizer
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python3 setup.py install
```
import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="threshold_optimizer", # Replace with your own username
    version="0.0.3",
    author="Gabriel Ziembicki",
    author_email="g.ziembicki@gmail.com",
    description="A package that optimizes threshold value based on probabilities and labels",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/gziembicki/threshold-optimizer",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)